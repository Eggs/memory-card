import React, {useEffect} from "react";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from '@material-ui/core/CircularProgress';

function CardArea(props) {
  const useStyles = makeStyles(theme => ({
    card: {
      maxHeight: 200,
      maxWidth: 300,
      '&:hover': {
        boxShadow: "0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)"
      }
    },
    cardActions: {
      padding: 0,
    },
  }));

  const classes = useStyles();
  return (
    <Grid container spacing={1}>
      {props.items.map(item => {
        return (
          <Grid key={props.items.indexOf(item)} item xs={3}>
            <Card className={classes.card} id={props.items.indexOf(item)} onClick={props.onClick}>
            <CardMedia className={classes.cardMedia}  height="100" width="150"><img src="http://placekitten.com/200/139"/> </CardMedia>
              <CardContent>
                <CardActions className={classes.cardActions}>
                    <Typography variant="body2">{item}</Typography>
                </CardActions>
              </CardContent>
            </Card>
          </Grid>
        );
      })}
    </Grid>
  );
}

export default CardArea;
