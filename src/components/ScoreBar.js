import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from "@material-ui/core/styles";

function ScoreBar(props) {
    const useStyles = makeStyles(theme => ({
      appBar: {
        marginBottom: 10,
      },
    }));

    const classes = useStyles();
    return (
      <AppBar className={classes.appBar} position="static">
        <Typography variant="h6">
          Score: {props.score}
        </Typography>
        <Typography variant="h6">
          Best Score: {props.bestScore}
        </Typography>
      </AppBar>
    )
}

export default ScoreBar;
