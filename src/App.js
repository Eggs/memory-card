import React, { useState } from "react";
import Container from "@material-ui/core/Container";

import ScoreBar from "./components/ScoreBar";
import CardArea from "./components/CardArea";

function App() {
  const [score, setScore] = useState(0);
  const [bestScore, setBestScore] = useState(0);
  const [selectedItems, setSelectedItems] = useState([]);

  const [items, setItems] = useState(["tea", "coffee", "chocolate", "lasagne", "guitar", "cup", "drums", "notebook"]);

  function handleItemClick(e) {
    const item = items[e.currentTarget.id]
    addSelectedItem(item);
    checkScore(item);
    checkBestScore();
    setItems(shuffleArray(items));
  }

  function checkScore(item){
    if(!checkSelectedItem(item)){
      setScore(score + 1);
    } else {
      setScore(0);
      resetSelectedItems();
    }
  }

  function checkBestScore() {
    if (score >= bestScore) {
      setBestScore(score);
    }
  }

  function addSelectedItem(item) {
    let newArr = [...selectedItems];
    newArr.push(item);
    setSelectedItems(newArr);
  }

  function checkSelectedItem(item) {
    return selectedItems.includes(item);
  }

  function resetSelectedItems(){
    setSelectedItems([]);
  }

  function shuffleArray(array) {
    let i = array.length - 1;
    for (; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
}

  return (
    <div className="App">
        <ScoreBar score={score} bestScore={bestScore}/>
        <CardArea items={items} onClick={handleItemClick}/>

    </div>
  );
}

export default App;
